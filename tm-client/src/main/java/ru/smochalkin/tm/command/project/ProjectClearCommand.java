package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-clear";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove all projects of current user.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        @NotNull final Result result = projectEndpoint.clearProjects(sessionService.getSession());
        printResult(result);
    }

}
