package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;

@Component
public class UserIsAuthCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "user-auth";
    }

    @Override
    @NotNull
    public String description() {
        return "Check user auth.";
    }

    @Override
    public void execute() {
        System.out.println("User auth: " + (sessionService.getSession() != null));
    }

}
