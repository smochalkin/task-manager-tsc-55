package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.smochalkin.tm.api.repository.dto.IProjectDtoRepository;
import ru.smochalkin.tm.api.repository.dto.ITaskDtoRepository;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

@Service
public final class ProjectTaskDtoService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public IProjectDtoRepository getProjectRepository() {
        return context.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    public ITaskDtoRepository getTaskRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.bindTaskById(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.unbindTaskById(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            return taskRepository.findTasksByUserIdAndProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }

    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeTasksByProjectId(id);
            projectRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            ProjectDto projectDto = projectRepository.findByName(userId, name);
            removeProjectById(projectDto.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            ProjectDto projectDto = projectRepository.findByIndex(userId, index);
            removeProjectById(projectDto.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
