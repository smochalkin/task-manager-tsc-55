package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-update-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("Enter new name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        @Nullable final String desc = TerminalUtil.nextLine();
        @NotNull final Result result = projectEndpoint.updateProjectById(sessionService.getSession(), id, name, desc);
        printResult(result);
    }

}
