package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.endpoint.ProjectDto;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-show-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final ProjectDto project = projectEndpoint.findProjectByName(sessionService.getSession(), name);
        showProject(project);
    }

}
