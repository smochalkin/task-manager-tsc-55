package ru.smochalkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractSystemCommand;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;

@Component
public final class AboutCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String arg() {
        return "-a";
    }

    @Override
    @NotNull
    public String name() {
        return "about";
    }

    @Override
    @NotNull
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println(propertyService.getDeveloperName());
        System.out.println(propertyService.getDeveloperEmail());
    }

}
