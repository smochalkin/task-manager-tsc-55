package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskUpdateStatusByNameCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-status-update-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Update task status by name.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusName = TerminalUtil.nextLine();
        taskEndpoint.changeTaskStatusByName(sessionService.getSession(), name, statusName);
    }

}
