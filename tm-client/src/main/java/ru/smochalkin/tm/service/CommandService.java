package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.api.service.ICommandService;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.AbstractSystemCommand;

import java.util.Collection;
import java.util.List;

@Service
public final class CommandService implements ICommandService {

    @Autowired
    private ICommandRepository commandRepository;

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    @NotNull
    public Collection<AbstractSystemCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    @NotNull
    public List<String> getCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    @NotNull
    public List<String> getCommandArgs() {
        return commandRepository.getCommandArgs();
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commandRepository.getCommandByName(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

}
