package ru.smochalkin.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.endpoint.*;
import ru.smochalkin.tm.exception.system.UnknownCommandException;
import ru.smochalkin.tm.repository.CommandRepository;
import ru.smochalkin.tm.service.*;
import ru.smochalkin.tm.util.SystemUtil;
import ru.smochalkin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractCommand[] commands;

    @Nullable
    private SessionDto session;

    private void init() {
        initPID();
        initCommands();
        fileScanner.init();
    }

    @SneakyThrows
    private void initCommands() {
        Arrays.stream(commands).forEach(command -> commandService.add(command));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager-client.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO CLIENT TASK MANAGER **");
        init();
        parseArgs(args);
        process();
    }

    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public void process() {
        logService.debug("process start...");
        @NotNull String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("Enter command: ");
            command = TerminalUtil.nextLine();
            logService.command(command);
            try {
                parseCommand(command);
                logService.info("`" + command + "` command executed");
            } catch (Exception e) {
                logService.error(e);
            }
            System.out.println();
        }
    }

    public void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException();
        command.execute();
    }

}
