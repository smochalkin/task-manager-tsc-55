package ru.smochalkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractSystemCommand;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;

import java.util.List;

@Component
public final class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String arg() {
        return "-arg";
    }

    @Override
    @NotNull
    public String name() {
        return "arguments";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final List<String> keys = commandService.getCommandArgs();
        for (String value : keys) {
            if (value == null || value.isEmpty()) continue;
            System.out.println(value);
        }
    }

}
