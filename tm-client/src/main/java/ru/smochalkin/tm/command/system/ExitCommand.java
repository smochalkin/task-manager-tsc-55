package ru.smochalkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractSystemCommand;

@Component
public final class ExitCommand extends AbstractSystemCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "exit";
    }

    @Override
    @NotNull
    public String description() {
        return "Close the program.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
