package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public class UserChangePassCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "user-change-password";
    }

    @Override
    @NotNull
    public String description() {
        return "Change user password.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter new password:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final Result result =  userEndpoint.userSetPassword(sessionService.getSession(), password);
        printResult(result);
    }

}

