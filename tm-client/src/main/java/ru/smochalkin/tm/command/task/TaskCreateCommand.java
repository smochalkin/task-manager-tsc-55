package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-create";
    }

    @Override
    @NotNull
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("Enter description: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final Result result = taskEndpoint.createTask(sessionService.getSession(), name, description);
        printResult(result);
    }

}
