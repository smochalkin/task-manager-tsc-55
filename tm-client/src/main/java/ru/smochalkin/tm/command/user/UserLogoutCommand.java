package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;

@Component
public class UserLogoutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "logout";
    }

    @Override
    @NotNull
    public String description() {
        return "Log out.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        @NotNull final Result result =  sessionEndpoint.closeSession(sessionService.getSession());
        printResult(result);
        sessionService.setSession(null);
    }

}
