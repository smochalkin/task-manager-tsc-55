package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskUpdateStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-status-update-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Update task status by index.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter index: ");
        @NotNull Integer index = TerminalUtil.nextInt();
        index--;
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusName = TerminalUtil.nextLine();
        @NotNull final Result result = taskEndpoint.changeTaskStatusByIndex(sessionService.getSession(), index, statusName);
        printResult(result);
    }

}
