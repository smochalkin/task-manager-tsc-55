package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final Result result = projectEndpoint.removeProjectById(sessionService.getSession(), projectId);
        printResult(result);
    }

}
