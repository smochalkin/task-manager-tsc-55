package ru.smochalkin.tm.exception.system;

import ru.smochalkin.tm.exception.AbstractException;

public final class RoleNotFoundException extends AbstractException {

    public RoleNotFoundException() {
        super("Error! Role option is incorrect.");
    }

}
