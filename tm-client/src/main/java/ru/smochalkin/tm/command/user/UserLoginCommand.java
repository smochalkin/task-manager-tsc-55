package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.SessionDto;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public class UserLoginCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "login";
    }

    @Override
    @NotNull
    public String description() {
        return "Log in.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final SessionDto session = sessionEndpoint.openSession(login, password);
        sessionService.setSession(session);
    }

}
